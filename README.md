# Minmlc - Compilateur de MinCaml en Java - Equipe BugPerture Science

Projet de compilation fait dans le cadre du Master 1 d'Informatique de l'Université Grenoble Alpes.

## Auteurs 

* BIZARD Astor
* BOUVIER-DENOIX Gabriel
* GRANIER Jonathan
* LAWSON Thibault
* WONG Noha



## Installation
Ce projet requiert :
- ant :
```bash
sudo apt install ant
```
- openJDK 8 :
```bash
sudo apt install openjdk-8-jdk
```
- JFlex :
```bash
sudo apt install jflex
```
- OCaml :
```bash
sudo apt install ocaml
```
- ARM compiler :
```bash
sudo apt install gcc-arm-none-eabi
sudo apt install qemu
```

## Compilation
```bash
#compile 
make

#clean 
make clean
```

## Usage
```bash
#Execution
cd scripts
./minmlc [options] <file.ml>

Available options :
	-o <file>           :       set the output file - "default.s" by default
	-h                  :       display this help
	-v                  :       display version
	-t                  :       do type check ONLY
	-p                  :       parse ONLY
	-asml <file>        :       set an output file for the generated asml code
	-ps                 :       print all the compilation and AST steps

	## Reductions ##
	-allR               :       do all reductions (K-Normalization, Alpha-Conversion, Beta-Reduction, Let-Reduction
	-kn                 :       do K-Normalization
	-ac                 :       do Alpha-Conversion
	-br                 :       do Beta-Reduction
	-lr                 :       do Let-Reduction

	## Optimisations ##
	-allO               :       do all optimisations (Inline Expansion, Constant Folding, Unused Definition Suppression
	-io                 :       do Inline Expansion Optimisation
	-io_c <v>           :       do Inline Expansion Optimisation with a function size threshold of v
	-cfo                :       do Constant Folding Optimisation
	-udo                :       do Unused Definition Suppression Optimisation
	-iter <v>           :       do all optimisations v times

	## BackEnd ##
	-as                 :       do asml translate
	-be                 :       do Back-End


```


## Test

### Lancer tous les tests

```bash
make test
```

*Note* : Il se peut que durant les tests, qemu provoque des erreurs de segmentation internes. La cause est inconnue et n'a pas été recherchée en profondeur.

### Tests unitaires 

Dans le dossier scripts 

`test_syntax.sh` teste syntaxiquement tous les fichiers ".ml" dans le dossier `tests/syntax/`
```bash
Usage : ./test_syntax [option]
Options :
    -h      :       afficher l'aide
    -v      :       s'arreter à la 1ere erreur et l'afficher 
```

`test_typecheck.sh` test le typage tous les fichiers ".ml" dans le dossier `tests/typechecking/`
```bash
Usage : ./test_typecheck [option]
Options :
    -h      :       afficher l'aide
    -v      :       s'arreter à la 1ere erreur et l'afficher 
```

`test_minmlc.sh` fait un test complet sur un fichier ".ml" , compilation , execution et comparaison avec l'éxécution ocaml
 ```bash
Usage : ./test_minmlc.sh <file.ml> [option]
Options :
    -h      :       afficher l'aide"
    -v      :       s'arreter à la 1ere erreur et l'afficher
```

`test_minmlc_all.sh` exécute le script `test_minmlc.sh` sur tous les fichiers de test (dossier `tests/gencode`)
```bash
Usage : ./test_minmlc_all.sh [option]
Options :
    -h      :       afficher l'aide
    -v      :       s'arreter à la 1ere erreur et l'afficher
```

`test_minmlc_asml.sh` fait un test complet sur un fichier ".ml" sur la génération asml - c'est un test du front-end
```bash
Usage : ./test_minmlc.sh <file.ml> [option]
Options :
    -h      :       afficher l'aide"
    -v      :       s'arreter à la 1ere erreur et l'afficher
```

`test_minmlc_asml_all.sh` exécute le script `test_minmlc_asml.sh` sur tous les fichiers de test (dossier `tests/gencode`)
```bash
Usage : ./test_minmlc_all.sh [option]
Options :
    -h      :       afficher l'aide
    -v      :       s'arreter à la 1ere erreur et l'afficher
```
